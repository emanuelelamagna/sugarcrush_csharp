﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SugarCrush_EmanueleLamagna.src.enums;
using SugarCrush_EmanueleLamagna.src.players;
using System;
using System.Collections.Generic;

namespace SugarCrush_EmanueleLamagna.src
{
    [TestClass]
    public class Test
    {
        [TestMethod]
        public void GeneralTest()
        {
            IPlayerManager pm = new PlayerManagerImpl();

            //test add players
            pm.AddPlayer("Emanuele");
            pm.AddPlayer("Filippo");
            pm.AddPlayer("Davide");
            IList<string> l = new List<string>();
            foreach (var dic in pm.GetPlayers())
            {
                l.Add(dic["player"].ToString());
            }
            Assert.IsTrue(l.Contains("Emanuele"));
            Assert.IsTrue(l.Contains("Filippo"));
            Assert.IsTrue(l.Contains("Davide"));

            //test update
            List<Dictionary<string, object>> list = pm.GetPlayers();
            foreach (var dic in list)
            {
                if (dic["player"].ToString().Equals("Filippo"))
                {
                    dic[StatsTypes.money.ToString()] = 3000;
                    dic[StatsTypes.level3Score.ToString()] = 24000;
                }
            }
            pm.Update(list);
            foreach (var dic in pm.GetPlayers())
            {
                if (dic["player"].ToString().Equals("Filippo"))
                {
                    Assert.IsTrue(Convert.ToInt32(dic[StatsTypes.money.ToString()])==3000);
                    Assert.IsTrue(Convert.ToInt32(dic[StatsTypes.level3Score.ToString()]) == 24000);
                }
            }

            //test remove players
            pm.RemovePlayer("Emanuele");
            pm.RemovePlayer("Filippo");
            pm.RemovePlayer("Davide");
            list = pm.GetPlayers();
            Assert.IsTrue(list.Count==0);

            //test double add of a player
            pm.AddPlayer("Emanuele");
            pm.AddPlayer("Emanuele");
            list = pm.GetPlayers();
            Assert.IsTrue(list.Count == 1);
        }
    }
}
