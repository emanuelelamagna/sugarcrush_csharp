﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SugarCrush_EmanueleLamagna.src.enums
{
    public enum StatsTypes
    {
        RED,
        YELLOW,
        BLUE,
        PURPLE,
        GREEN,
        ORANGE,
        FRECKLES,
        STRIPED,
        WRAPPED,
        CHOCOLATE,
        money,
        totalScore,
        level1Score,
        level2Score,
        level3Score,
        level4Score,
        level5Score,
        level6Score,
        level7Score,
        level8Score,
        level9Score,
        level10Score
    }
}
