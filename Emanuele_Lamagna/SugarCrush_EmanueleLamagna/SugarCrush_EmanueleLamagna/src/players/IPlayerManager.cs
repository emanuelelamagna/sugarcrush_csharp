﻿using System.Collections.Generic;

namespace SugarCrush_EmanueleLamagna.src.players
{
    public interface IPlayerManager
    {
        void AddPlayer(string name);
        List<Dictionary<string, object>> GetPlayers();
        void Update(List<Dictionary<string, object>> list);
        void RemovePlayer(string name);
    }
}
