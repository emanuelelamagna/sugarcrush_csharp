﻿using Newtonsoft.Json.Linq;
using SugarCrush_EmanueleLamagna.src.enums;
using System;
using System.Collections.Generic;
using System.IO;

namespace SugarCrush_EmanueleLamagna.src.players
{
    public class PlayerManagerImpl : IPlayerManager
    {
        private static readonly string player = "player";
        private static readonly string folderPath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) +
                Path.DirectorySeparatorChar + ".sugarcrush_csharp";
        private static readonly string filePath = folderPath + Path.DirectorySeparatorChar + "players.json";
        private JArray js;

        public void AddPlayer(string name)
        {
            CheckFiles();

            using (StreamReader sw = new StreamReader(filePath))
            {
                js = JArray.Parse(File.ReadAllText(filePath));
                sw.Close();
            }
            bool isPresent = false;
            foreach (JObject job in js)
            {
                if (job.GetValue(player).Value<string>().Equals(name))
                {
                    isPresent = true;
                }
            }
            if (!isPresent)
            {
                JObject jo = new JObject
                {
                    { player, name }
                };
                IList<string> list = Enum.GetNames(typeof(StatsTypes));
                foreach (string value in list)
                {
                    jo.Add(value, 0);
                }
                js.Add(jo);
                using (StreamWriter sw = new StreamWriter(filePath))
                {
                    sw.WriteLine(js);
                }
            }
        }

        public List<Dictionary<string, object>> GetPlayers()
        {
            CheckFiles();
            List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();

            using (StreamReader sw = new StreamReader(filePath))
            {
                js = JArray.Parse(File.ReadAllText(filePath));
                sw.Close();
            }
            foreach (JObject ob in js)
            {
                Dictionary<string, object> d = new Dictionary<string, object>();
                foreach (var e in ob)
                {
                    d.Add(e.Key, e.Value);
                }
                list.Add(d);
            }

            return list;
        }

        public void Update(List<Dictionary<string, object>> list)
        {
            CheckFiles();
            JArray tmp = new JArray();
            foreach (var dic in list)
            {
                JObject jo = new JObject();
                foreach (var k in dic.Keys)
                {
                    jo.Add(k, dic[k].ToString());
                }
                tmp.Add(jo);
            }
            js = tmp;
            using (StreamWriter sw = new StreamWriter(filePath))
            {
                sw.WriteLine(js);
            }
        }

        public void RemovePlayer(string name)
        {
            CheckFiles();
            using (StreamReader sw = new StreamReader(filePath))
            {
                js = JArray.Parse(File.ReadAllText(filePath));
                sw.Close();
            }
            JObject id = null;
            foreach (JObject jo in js)
            {
                if (jo.GetValue(player).Value<string>().Equals(name))
                {
                    id = jo;
                }
            }
            if (id!=null)
            {
                js.Remove(id);
            }

            foreach (var elem in js)
            {
                Console.WriteLine(elem[player]);
            }

            using (StreamWriter sw = new StreamWriter(filePath))
            {
                sw.WriteLine(js);
            }
        }

        private void CheckFiles()
        {
            if (!File.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            if (!File.Exists(filePath))
            {
               var f = File.Create(filePath);
               f.Close();
                using (StreamWriter sw = new StreamWriter(filePath))
                {
                    sw.WriteLine("[]");
                }
            }

        }
    }
}
