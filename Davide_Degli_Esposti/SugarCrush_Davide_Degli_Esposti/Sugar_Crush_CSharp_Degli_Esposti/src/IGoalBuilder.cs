﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sugar_Crush_CSharp_Degli_Esposti
{
    interface IGoalBuilder
    {
        /// <summary>
        /// Sets the title of the achievement, can't be null
        /// </summary>
        /// <param name="title"></param>
        /// <returns> this istance of IGoalBuilder </returns>
        IGoalBuilder SetTitle(String title);

        /// <summary>
        /// Sets the description of the achievement, can't be null
        /// </summary>
        /// <param name="descr"></param>
        /// <returns> this instance of GoalBuilder</returns>
        IGoalBuilder SetDescr(String descr);

        /// <summary>
        /// Set the method used to check if the goal is reached
        /// </summary>
        /// <param name="method"></param>
        /// <returns> this istance of IGoalBuilder </returns>
        IGoalBuilder SetMethod(Predicate<IDictionary<String, Object>> method);

        /// <summary>
        /// If everything is ok, it creates the goal
        /// </summary>
        /// <returns> a new object Goal </returns>
        Goal Build();
    }
}
