﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sugar_Crush_CSharp_Degli_Esposti
{
    class Goal
    {
        private String title;
        private String descr;
        private Boolean reached;
        private Predicate<IDictionary<String, Object>> method;

        public Goal(String title,String descr,Predicate<IDictionary<String,Object>> method)
        {
            this.title = title;
            this.descr = descr;
            this.reached = false;
            this.method = method;

        }

        /// <summary>
        /// get the title of the goal
        /// </summary>
        /// <returns> title </returns>
        public String GetTitle()
        {
            return this.title;
        }

        /// <summary>
        /// get the description of the goal
        /// </summary>
        /// <returns> descr </returns>
        public String GetDescr()
        {
            return this.descr;
        }

        /// <summary>
        /// get true if the goal is reached
        /// </summary>
        /// <returns> reached </returns>
        public Boolean IsReached()
        {
            return this.reached;
        }

        /*
        public Boolean CheckIfReached()
        {

        }
        */
    }
}
