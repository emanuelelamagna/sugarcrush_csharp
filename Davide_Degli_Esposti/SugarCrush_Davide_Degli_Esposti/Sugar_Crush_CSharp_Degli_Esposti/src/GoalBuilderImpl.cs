﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Sugar_Crush_CSharp_Degli_Esposti
{
    class GoalBuilderImpl: IGoalBuilder
    {
        private String title;
        private String descr;
        private Predicate<IDictionary<String, Object>> method;
        private Boolean isBuilt = false;

        public IGoalBuilder SetTitle(String title)
        {
            if(title.Equals(""))
            {
                throw new ArgumentException("Title can't be null");
            }
            this.title = title;
            return this;
        }

        public IGoalBuilder SetDescr(String descr)
        {
            if(descr.Equals(""))
            {
                throw new ArgumentException("Description can't be null");
            }
            this.descr = descr;
            return this;
        }

        public IGoalBuilder SetMethod(Predicate<IDictionary<String, Object>> method)
        {
            if (method == null)
            {
                throw new NullReferenceException("The method can't be null");
            }
            this.method = method;
            return this;
        }

        public Goal Build()
        {
            if (this.isBuilt)
            {
                throw new ArgumentException("Can't build the same goal twice.");
            }
            if (this.title == null)
            {
                throw new NullReferenceException("Title not set.");
            }
            if (this.descr == null)
            {
                throw new NullReferenceException("Description not set.");
            }
            if (this.method == null)
            {
                throw new NullReferenceException("Method not set.");
            }
            this.isBuilt = true;
            return new Goal(this.title, this.descr, this.method);
        }
    }
}
