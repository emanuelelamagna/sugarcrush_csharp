﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace Sugar_Crush_CSharp_Degli_Esposti.src
{
    [TestClass]
    public class TestGoalBuilder
    {
        private IGoalBuilder gb;
       
       
        public void Prepare()
        {
            this.gb = new GoalBuilderImpl();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        //test SetTitle method
        public void TitleCantBeAVoidString()
        {
            Prepare();
            this.gb.SetTitle("");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        //test SetDescr method
        public void DescrCantBeAVoidString()
        {
            Prepare();
            this.gb.SetDescr("");
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        //test SetMethod method
        public void MethodCantBeNull()
        {
            Prepare();
            this.gb.SetMethod(null);
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        //test Build method
        public void TitleNeedToBeSet()
        {
            Prepare();
            this.gb.SetDescr("Questa è una descrizione");
            this.gb.SetMethod(e => false);
            this.gb.Build();
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        //test Build method
        public void DescrNeedToBeSet()
        {
            Prepare();
            this.gb.SetTitle("Questo è un titolo");
            this.gb.SetMethod(e => true);
            this.gb.Build();
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        //test Build method
        public void MethodNeedToBeSet()
        {
            Prepare();
            this.gb.SetTitle("Questo è un titolo");
            this.gb.SetDescr("Questa è una descrizione");
            this.gb.Build();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        //test Build method
        public void CantBuiltTwice()
        {
            Prepare();
            this.gb.SetTitle("Questo è un titolo");
            this.gb.SetDescr("Questa è una descrizione");
            this.gb.SetMethod(e => false);
            this.gb.Build();
            this.gb.Build();
        }
        
    }
}
