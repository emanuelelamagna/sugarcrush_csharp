﻿using System;

namespace SugarCrush_Csharp
{
    /// <author> Filippo Barbari </author>
    public interface IStage
    {

        
        ///<returns>The message that needs to be printed at the beginning of the Stage.
        ///         If an empty string is returned, no message is to be printed.</returns>
        String GetStartingMessage();

        ///<returns>The message that needs to be printed after the end of the Stage.
        ///         If an empty string is returned, no message is to be printed.</returns>
        String GetEndingMessage();
    }

}