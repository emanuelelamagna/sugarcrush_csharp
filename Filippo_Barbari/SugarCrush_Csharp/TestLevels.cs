﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace SugarCrush_Csharp
{
    [TestClass]
    public class TestLevels
    {

        [TestMethod]
        public void generalTest()
        {
            string startMsg = "Benvenuto";
            string endMsg = "Arrivederci";

            IStage s1 = new Stage(startMsg, endMsg);

            //Testing stage's consistency
            Assert.IsTrue(s1.GetStartingMessage().Equals(startMsg));
            Assert.IsTrue(s1.GetEndingMessage().Equals(endMsg));

            IStage s2 = new Stage("Hello", "Goodbye");

            List<IStage> ll = new List<IStage>{s1, s2};

            ILevel lvl = new Level(ll);

            //Testing iteration through stages
            Assert.IsTrue(lvl.MoveNext());
            Assert.IsTrue(lvl.Current().Equals(s1));
            Assert.IsTrue(lvl.MoveNext());
            Assert.IsTrue(lvl.Current().Equals(s2));
            Assert.IsFalse(lvl.MoveNext());
        }
    }
}
