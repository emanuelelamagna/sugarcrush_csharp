﻿namespace SugarCrush_Csharp
{
    /// <summary>
    /// An interface to manage all available Levels.
    /// Filippo Barbari
    /// </summary>
    public interface ILevelsManager
    {

        /// <param name="levelNumber"> The number of level to be returned. </param>
        /// <returns> The level corresponding to the given number. </returns>
        ILevel GetLevel(int levelNumber);

        ///<returns> The Levels representing the tutorial. </returns>
        ILevel GetTutorial();

        ///<returns> The number of available levels excluding tutorial. </returns>
        int GetNumLevels();

    }
}
