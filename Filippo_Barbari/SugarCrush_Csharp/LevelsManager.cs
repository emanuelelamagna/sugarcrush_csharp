﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SugarCrush_Csharp
{
    /// <summary>
    /// A basic implementation of ILevelsManager.
    /// </summary>
    /// <author>Filippo Barbari</author>
    public class LevelsManager : ILevelsManager
    {

        /// <summary>
        /// Instead of saving directly the {@link Level}, i save a {@link Supplier}
        /// with the construction of the {@link Level}.
        /// Saving {@link Level}s this way optimizes memory usage.
        /// </summary>
        private IDictionary<int, Func<Level>> normalLevels = new Dictionary<int, Func<Level>>();
        private Func<Level> tutorial = null;

        public ILevel GetLevel(int levelNumber)
        {
            if (levelNumber <= 0 || !this.normalLevels.ContainsKey(levelNumber))
            {
                throw new ArgumentException("Level number not valid.");
            }

            return this.normalLevels.ElementAt(levelNumber).Value.Invoke();
        }

        public ILevel GetTutorial()
        {
                return tutorial.Invoke();
        }

        public int GetNumLevels()
        {
                return this.normalLevels.Count;
        }

        public override int GetHashCode()
        {
            int prime = 31;
            int result = 1;
            result = prime * result + ((normalLevels == null) ? 0 : normalLevels.GetHashCode());
            result = prime * result + ((tutorial == null) ? 0 : tutorial.GetHashCode());
            return result;
        }

        public override bool Equals(Object obj)
        {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (GetType() != obj.GetType())
                return false;
            LevelsManager other = (LevelsManager)obj;
            if (normalLevels == null)
            {
                if (other.normalLevels != null)
                    return false;
            }
            else if (!normalLevels.Equals(other.normalLevels))
                return false;
            if (tutorial == null)
            {
                if (other.tutorial != null)
                    return false;
            }
            else if (!tutorial.Equals(other.tutorial))
                return false;
            return true;
        }

        public override String ToString()
        {
            return "LevelsManager [normalLevels=" + normalLevels.ToString() + ", tutorial=" + tutorial.ToString() + "]";
        }

    }
}
