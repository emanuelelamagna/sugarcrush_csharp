﻿namespace SugarCrush_Csharp
{
    /// <summary>
    /// The interface that allows to interact with a Level of the game.
    /// </summary>
    public interface ILevel : IStage
    {

        /// Moves to the next Stage in the collection.
        /// <returns> True if end of collection has not been reached,
        ///           false otherwise. </returns>
        bool MoveNext();

        /// <returns> The current stage. </returns>
        IStage Current();
    }
}