﻿using System;

namespace SugarCrush_Csharp
{
    public class Stage : IStage
    {


        private String startingMessage;
        private String endingMessage;

        public Stage(String startMsg, String endMsg)
        {
            if(startMsg == null || endMsg == null)
            {
                throw new NullReferenceException();
            }
            this.startingMessage = startMsg;
            this.endingMessage = endMsg;
        }

        public String GetStartingMessage() {
            return this.startingMessage;
        }

        public String GetEndingMessage() {
            return this.endingMessage;
        }
    }

}
