﻿using System;
using System.Collections.Generic;

namespace SugarCrush_Csharp
{
    /// <summary>
    /// A basic implementation of a level.
    /// </summary>
    class Level : ILevel
    {
        private IEnumerator<IStage> itStage;

        public Level(List<IStage> stages)
        {
            if(stages == null)
            {
                throw new NullReferenceException();
            }
            this.itStage = stages.GetEnumerator();
        }

        private void AssertCurrentStageSet()
        {
            if (itStage.Current == null)
            {
                throw new InvalidOperationException("No current state present.");
            }
        }

        public bool MoveNext()
        {
            return itStage.MoveNext();
        }

        public IStage Current()
        {
            return itStage.Current;
        }

        public String GetStartingMessage() {
            AssertCurrentStageSet();
            return itStage.Current.GetStartingMessage();
        }

        public String GetEndingMessage() {
            AssertCurrentStageSet();
            return itStage.Current.GetEndingMessage();
        }
    }
}
