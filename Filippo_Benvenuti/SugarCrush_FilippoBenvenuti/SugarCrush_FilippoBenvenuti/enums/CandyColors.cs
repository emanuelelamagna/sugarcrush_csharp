﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SugarCrush_FilippoBenvenuti.enums
{
	/// <summary>
	/// Every color of candy in the game.
	/// </summary>
	public enum CandyColors
	{
		RED,
		ORANGE,
		YELLOW,
		GREEN,
		BLUE,
		PURPLE,
		FRECKLES,
		CHOCOLATE
	}
}
