﻿using SugarCrush_FilippoBenvenuti.enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SugarCrush_FilippoBenvenuti.grid
{
	/// <summary>
	/// Representing an immutable candy.
	/// </summary>
	class Candy
	{
		private CandyTypes _type;
		private CandyColors _color;

		public Candy(CandyTypes type, CandyColors color)
		{
			this._type = type;
			this._color = color;
		}

		public CandyTypes _Type { get => _type; private set => _type = value; }
		public CandyColors _Color { get => _color; private set => _color = value; }

		public override bool Equals(object obj)
		{
			return this._type == ((Candy)obj)._type && this._color == ((Candy)obj)._color;
		}

		public override int GetHashCode()
		{
			return _type.GetHashCode() ^ _color.GetHashCode();
		}
	}
}
