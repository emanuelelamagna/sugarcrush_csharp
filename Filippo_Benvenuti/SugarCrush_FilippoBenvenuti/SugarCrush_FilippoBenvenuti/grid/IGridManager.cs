﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SugarCrush_FilippoBenvenuti.grid
{
	interface IGridManager
	{
		/// <summary>
		/// Make two candies being swapped.
		/// </summary>
		/// <param name="first"> The first candy to be swap. </param>
		/// <param name="second"> The second candy to be swap. </param>
		/// <returns> True if move was possible. </returns>
		bool Move(Point first, Point second);

		/// <summary>
		/// Mutate the candy in coord position into passed candy.
		/// </summary>
		/// <param name="coord"> Coordinated of candy to mutate. </param>
		/// <param name="candy"> The new candy. </param>
		/// <returns> True if mutate was possible. </returns>
		bool MutateCandy(Point coord, Candy candy);

		/// <summary>
		/// Retrieves the dictionary of candy in the grid.
		/// </summary>
		/// <returns></returns>
		Dictionary<Point, Candy> GetGrid();
	}
}
