﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SugarCrush_FilippoBenvenuti.grid
{
	class GridManager : IGridManager
	{
		private Dictionary<Point, Candy> grid;

		public GridManager(Dictionary<Point, Candy> grid)
		{
			if(grid == null)
				throw new Exception("Grid can't be null");
			this.grid = new Dictionary<Point, Candy>(grid);
		}

		public Dictionary<Point, Candy> GetGrid()
		{
			return new Dictionary<Point, Candy>(this.grid);
		}

		public bool Move(Point first, Point second)
		{
			Trace.WriteLine(first.X + " " + first.Y);
			Trace.WriteLine(second.X + " " + second.Y + "\n");
			if ((!this.grid.ContainsKey(first)) || (!this.grid.ContainsKey(second)))
				return false;
			if (Point.Distance(first, second) > 1)
				return false;
			var tmp = this.grid[first];
			this.grid[first] = this.grid[second];
			this.grid[second] = tmp;
			return true;
		}

		public bool MutateCandy(Point coord, Candy candy)
		{
			if (!this.grid.ContainsKey(coord))
				return false;
			this.grid[coord] = candy;
			return true;
		}
	}
}
