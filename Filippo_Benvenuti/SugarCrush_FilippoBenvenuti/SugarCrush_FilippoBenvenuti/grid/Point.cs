﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SugarCrush_FilippoBenvenuti.grid
{
	/// <summary>
	/// Short container of coordinates.
	/// </summary>
	class Point
	{
		private int _x;
		private int _y;

		public Point(int x, int y)
		{
			this._x = x;
			this._y = y;
		}

		public int X { get => _x; set => _x = value; }
		public int Y { get => _y; set => _y = value; }

		static public double Distance(Point a, Point b)
		{
			return Math.Sqrt(Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2));
		}

		public override bool Equals(object obj)
		{
			return this._x == ((Point)obj)._x && this._y == ((Point)obj)._y;
		}

		public override int GetHashCode()
		{
			return _x.GetHashCode() ^ _y.GetHashCode();
		}
	}
}
