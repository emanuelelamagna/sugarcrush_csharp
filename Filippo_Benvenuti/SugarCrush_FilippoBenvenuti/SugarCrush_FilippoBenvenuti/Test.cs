﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SugarCrush_FilippoBenvenuti.grid;

namespace SugarCrush_FilippoBenvenuti
{
	[TestClass]
	public class Test
	{
		[TestMethod]
		[ExpectedException(typeof(Exception), "Grid can't be null")]
		public void CantBeNull()
		{
			new GridManager(null);
		}

		[TestMethod]
		public void TestMove()
		{
			var grid = new GridManager(new Dictionary<Point, Candy>()
			{
				[new Point(0, 0)] = new Candy(enums.CandyTypes.NORMAL, enums.CandyColors.BLUE),
				[new Point(0, 1)] = new Candy(enums.CandyTypes.NORMAL, enums.CandyColors.GREEN),
				[new Point(1, 1)] = new Candy(enums.CandyTypes.NORMAL, enums.CandyColors.RED)
			});
			Assert.IsFalse(grid.Move(new Point(0, 0), new Point(0, -1)));
			Assert.IsFalse(grid.Move(new Point(0, 0), new Point(1, 1)));
			Assert.IsTrue(grid.Move(new Point(0, 0), new Point(0, 1)));
		}

		[TestMethod]
		public void TestMutate()
		{
			var grid = new GridManager(new Dictionary<Point, Candy>()
			{
				[new Point(0, 0)] = new Candy(enums.CandyTypes.NORMAL, enums.CandyColors.BLUE),
				[new Point(0, 1)] = new Candy(enums.CandyTypes.NORMAL, enums.CandyColors.GREEN),
				[new Point(1, 1)] = new Candy(enums.CandyTypes.NORMAL, enums.CandyColors.RED)
			});
			Assert.IsFalse(grid.MutateCandy(new Point(0, -1), new Candy(enums.CandyTypes.NORMAL, enums.CandyColors.GREEN)));
			Assert.IsTrue(grid.MutateCandy(new Point(0, 1), new Candy(enums.CandyTypes.NORMAL, enums.CandyColors.GREEN)));
			Assert.IsTrue(grid.GetGrid()[new Point(0, 1)].Equals(new Candy(enums.CandyTypes.NORMAL, enums.CandyColors.GREEN)));
		}
	}
}
